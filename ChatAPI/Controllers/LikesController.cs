using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ChatAPI.Models.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace ChatAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LikesController : ControllerBase
    {
        private readonly ChatContext _context;

        public LikesController(ChatContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Adds a like on the given message from the given user
        /// 
        /// This endpoint is idempotent
        /// </summary>
        /// <param name="input"></param>
        /// <returns>204 if successful, 400 if user or message does not exist or like already exists</returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult> AddLike([Required] LikeInput input)
        {
            if (!await _context.Messages.AnyAsync(x => x.ID == input.MessageID))
            {
                return BadRequest(new ProblemDetails
                {
                    Title  = "Message does not exist",
                    Detail = "There is no message with the given ID"
                });
            }

            if (!await _context.Users.AnyAsync(x => x.ID == input.UserID))
            {
                return BadRequest(new ProblemDetails
                {
                    Title  = "User does not exist",
                    Detail = "There is no user with the given ID"
                });
            }

            var existingLike = await GetExistingLike(input);

            if (existingLike == null)
            {
                _context.Likes.Add(new Like
                {
                    MessageID = input.MessageID,
                    UserID    = input.UserID
                });
                await _context.SaveChangesAsync();

                return NoContent();
            }
            else
            {
                return BadRequest(new ProblemDetails()
                {
                    Title  = "Entity already exists",
                    Detail = "A like from the user for the given message already exists"
                });
            }
        }

        /// <summary>
        /// Deletes a like on the given message from the given user
        /// </summary>
        /// <param name="input">The message ID and user ID for the like</param>
        /// <returns>204</returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult> DeleteLike([Required] LikeInput input)
        {
            var like = await GetExistingLike(input);

            if (like != null)
            {
                _context.Remove(like);
                await _context.SaveChangesAsync();

                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        private Task<Like> GetExistingLike(LikeInput input)
        {
            return _context.Likes
                .SingleOrDefaultAsync(x => x.MessageID == input.MessageID && x.UserID == input.UserID);
        }

        public class LikeInput
        {
            public Guid MessageID { get; set; }
            public int  UserID    { get; set; }
        }
    }
}