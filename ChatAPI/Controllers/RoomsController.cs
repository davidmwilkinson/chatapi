using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ChatAPI.Models.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace ChatAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RoomsController : ControllerBase
    {
        private readonly ChatContext _context;

        public RoomsController(ChatContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Returns a collection of all rooms, ordered by name
        /// </summary>
        /// <param name="includeClosed">Optional parameter to include closed rooms</param>
        /// <returns>200 containing a collection of rooms</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<IEnumerable<RoomOutput>>> GetAllRooms([FromQuery] bool includeClosed = false)
        {
            var query = _context.Rooms
                .OrderBy(x => x.Name.ToLower())
                .AsQueryable();

            if (!includeClosed) query = query.Where(x => !x.Closed);

            var rooms = await query
                .Select(x => new RoomOutput(x))
                .ToListAsync();

            return rooms;
        }

        /// <summary>
        /// Gets a single room with the given ID
        /// </summary>
        /// <param name="id">The room ID</param>
        /// <returns>200 containing the room </returns>
        [HttpGet("{id}", Name = "GetRoom")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<RoomOutput>> GetRoom(int id)
        {
            var room = await _context.Rooms.SingleOrDefaultAsync(x => x.ID == id);

            if (room == null) return NotFound();

            return new RoomOutput(room);
        }

        /// <summary>
        /// Creates a room with the given name. The name may be the same as an existing room. Maximum name length is 250 characters
        /// </summary>
        /// <param name="input">Name and description of room</param>
        /// <returns>201 containing the created room</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult> CreateRoom([Required] [FromBody] CreateRoomInput input)
        {
            var room = new Room
            {
                DateCreated = DateTime.Now,
                Name        = input.Name,
                Closed      = false,
                Description = input.Description
            };

            _context.Rooms.Add(room);

            await _context.SaveChangesAsync();

            return CreatedAtRoute("GetRoom", new {id = room.ID}, room);
        }

        /// <summary>
        /// Closes the room with the given ID
        /// </summary>
        /// <param name="id">The room ID</param>
        /// <returns>204 if successful, 404 if room is not found</returns>
        [HttpDelete("Close/{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult> CloseRoom([Required] int id)
        {
            var room = await _context.Rooms
                .SingleOrDefaultAsync(x => x.ID == id && x.Closed == false);

            if (room == null) return NotFound();

            room.Closed     = true;
            room.DateClosed = DateTime.Now;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// The name and description of the room to be created
        /// </summary>
        public class CreateRoomInput
        {
            [Required]
            [StringLength(Constants.MaxRoomNameLength, MinimumLength = 1)]
            public string Name { get; set; }

            [MinLength(1)]
            public string Description { get; set; }
        }

        public class RoomOutput
        {
            public int       ID          { get; set; }
            public string    Name        { get; set; }
            public string    Description { get; set; }
            public DateTime  DateCreated { get; set; }
            public bool      Closed      { get; set; }
            public DateTime? DateClosed  { get; set; }

            public RoomOutput(Room room)
            {
                ID          = room.ID;
                Name        = room.Name;
                Description = room.Description;
                DateCreated = room.DateCreated;
                Closed      = room.Closed;
                DateClosed  = room.DateClosed;
            }
        }
    }
}