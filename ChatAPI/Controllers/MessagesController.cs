using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ChatAPI.Models.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace ChatAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessagesController : ControllerBase
    {
        private readonly ChatContext _context;

        public MessagesController(ChatContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Creates a message from the given user in the given room
        /// </summary>
        /// <param name="input"></param>
        /// <returns>200 containing the created message</returns>
        [HttpPost]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesDefaultResponseType]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<MessageOutput>> CreateMessage([FromBody] [Required] MessageInput input)
        {
            if (!await _context.Rooms.AnyAsync(x => x.ID == input.RoomID
                                                    && x.Closed == false))
            {
                return BadRequest(new ProblemDetails
                {
                    Title  = "Room does not exist",
                    Detail = "There is not an open room with the given ID"
                });
            }

            if (!await _context.Users.AnyAsync(x => x.ID == input.UserID))
            {
                return BadRequest(new ProblemDetails
                {
                    Title  = "User does not exist",
                    Detail = "There is no user with the given ID"
                });
            }

            var message = new Message
            {
                Content = input.Content,
                RoomID  = input.RoomID,
                UserID  = input.UserID
            };

            _context.Messages.Add(message);
            await _context.SaveChangesAsync();

            return new MessageOutput(message);
        }

        /// <summary>
        /// Gets the messages in the given room ordered by date created (descending), optionally between the given dates.
        ///
        /// If fetching for an ID for a closed room, it will return 
        /// </summary>
        /// <param name="roomID">The room ID</param>
        /// <param name="after">Optional beginning UTC time</param>
        /// <param name="before">Optional ending UTC time</param>
        /// <returns>A collection of messages in the room</returns>
        [HttpGet("ForRoom/{roomID}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<IEnumerable<MessageOutput>>> GetRoomMessages(int                   roomID,
                                                                                    [FromQuery] DateTime? after  = null,
                                                                                    [FromQuery] DateTime? before = null)
        {
            // TODO impose a number limit and require the client to make more requests using the before and after params
            if (before != null
                && after != null
                && before < after)
            {
                return BadRequest(new ProblemDetails()
                {
                    Title  = "Time span invalid",
                    Detail = "The given 'after' and 'before' flags are invalid together"
                });
            }

            if (!await _context.Rooms.AnyAsync(x => x.ID == roomID))
            {
                return BadRequest(new ProblemDetails
                {
                    Title  = "Room does not exist",
                    Detail = "There is no room with the given ID"
                });
            }

            var query = _context.Messages
                .Include(x => x.Likes)
                .ThenInclude(x => x.User)
                .Include(x => x.User)
                .Where(x => x.RoomID == roomID && x.Room.Closed == false);

            if (after != null) query = query.Where(x => x.DateCreated > after);

            if (before != null) query = query.Where(x => x.DateCreated < before);

            return await query
                .OrderByDescending(x => x.DateCreated)
                .Select(message => new MessageOutput(message))
                .ToListAsync();
        }

        public class MessageOutput
        {
            public Guid                             ID          { get; set; }
            public string                           Content     { get; set; }
            public DateTime                         DateCreated { get; set; }
            public MessagesUserOutput               User        { get; set; }
            public IEnumerable<MessagesLikesOutput> Likes       { get; set; }

            public MessageOutput(Message message)
            {
                ID          = message.ID;
                Content     = message.Content;
                DateCreated = message.DateCreated;
                Likes = message.Likes?.Select(like => new MessagesLikesOutput
                {
                    User = new MessagesUserOutput {ID = like.UserID}
                });
                User = new MessagesUserOutput {ID = message.UserID};
            }
        }

        public class MessagesUserOutput
        {
            public int ID { get; set; }
        }

        public class MessagesLikesOutput
        {
            public MessagesUserOutput User { get; set; }
        }

        public class MessageInput
        {
            [Required]
            [MinLength(1)]
            public string Content { get; set; }

            public int RoomID { get; set; }

            public int UserID { get; set; }
        }
    }
}