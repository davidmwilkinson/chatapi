﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ChatAPI.Migrations
{
    public partial class AddedDateGeneration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                "DateCreated",
                "Rooms",
                nullable: false,
                defaultValueSql: "current_timestamp",
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                "DateCreated",
                "Messages",
                nullable: false,
                defaultValueSql: "current_timestamp",
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                "DateLiked",
                "Likes",
                nullable: false,
                defaultValueSql: "current_timestamp",
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -3,
                "DateCreated",
                new DateTime(2019,
                             12,
                             1,
                             17,
                             43,
                             0,
                             38,
                             DateTimeKind.Local).AddTicks(1139));

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -2,
                "DateCreated",
                new DateTime(2019,
                             11,
                             30,
                             18,
                             43,
                             0,
                             38,
                             DateTimeKind.Local).AddTicks(1081));

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -1,
                "DateCreated",
                new DateTime(2019,
                             12,
                             1,
                             18,
                             43,
                             0,
                             35,
                             DateTimeKind.Local).AddTicks(1115));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                "DateCreated",
                "Rooms",
                "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldDefaultValueSql: "current_timestamp");

            migrationBuilder.AlterColumn<DateTime>(
                "DateCreated",
                "Messages",
                "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldDefaultValueSql: "current_timestamp");

            migrationBuilder.AlterColumn<DateTime>(
                "DateLiked",
                "Likes",
                "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldDefaultValueSql: "current_timestamp");

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -3,
                "DateCreated",
                new DateTime(2019,
                             12,
                             1,
                             17,
                             29,
                             27,
                             324,
                             DateTimeKind.Local).AddTicks(3722));

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -2,
                "DateCreated",
                new DateTime(2019,
                             11,
                             30,
                             18,
                             29,
                             27,
                             324,
                             DateTimeKind.Local).AddTicks(3663));

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -1,
                "DateCreated",
                new DateTime(2019,
                             12,
                             1,
                             18,
                             29,
                             27,
                             321,
                             DateTimeKind.Local).AddTicks(4224));
        }
    }
}