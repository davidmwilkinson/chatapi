﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ChatAPI.Migrations
{
    public partial class RemovedRoomUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "RoomUser");

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -3,
                "DateCreated",
                new DateTime(2019,
                             12,
                             1,
                             23,
                             28,
                             45,
                             992,
                             DateTimeKind.Local).AddTicks(1297));

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -2,
                "DateCreated",
                new DateTime(2019,
                             12,
                             1,
                             0,
                             28,
                             45,
                             992,
                             DateTimeKind.Local).AddTicks(1242));

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -1,
                "DateCreated",
                new DateTime(2019,
                             12,
                             2,
                             0,
                             28,
                             45,
                             989,
                             DateTimeKind.Local).AddTicks(1692));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "RoomUser",
                table => new
                {
                    UserID = table.Column<int>("integer", nullable: false),
                    RoomID = table.Column<int>("integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomUser", x => new {x.UserID, x.RoomID});
                    table.ForeignKey(
                        "FK_RoomUser_Rooms_RoomID",
                        x => x.RoomID,
                        "Rooms",
                        "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_RoomUser_Users_UserID",
                        x => x.UserID,
                        "Users",
                        "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                "RoomUser",
                new[] {"UserID", "RoomID"},
                new object[,]
                {
                    {-1, -3},
                    {-2, -3},
                    {-3, -3}
                });

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -3,
                "DateCreated",
                new DateTime(2019,
                             12,
                             1,
                             17,
                             43,
                             0,
                             38,
                             DateTimeKind.Local).AddTicks(1139));

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -2,
                "DateCreated",
                new DateTime(2019,
                             11,
                             30,
                             18,
                             43,
                             0,
                             38,
                             DateTimeKind.Local).AddTicks(1081));

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -1,
                "DateCreated",
                new DateTime(2019,
                             12,
                             1,
                             18,
                             43,
                             0,
                             35,
                             DateTimeKind.Local).AddTicks(1115));

            migrationBuilder.CreateIndex(
                "IX_RoomUser_RoomID",
                "RoomUser",
                "RoomID");
        }
    }
}