﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ChatAPI.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:uuid-ossp", ",,");

            migrationBuilder.CreateTable(
                "Rooms",
                table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name        = table.Column<string>(maxLength: 250, nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    Disabled    = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Rooms", x => x.ID); });

            migrationBuilder.CreateTable(
                "Users",
                table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                },
                constraints: table => { table.PrimaryKey("PK_Users", x => x.ID); });

            migrationBuilder.CreateTable(
                "Messages",
                table => new
                {
                    ID          = table.Column<Guid>(nullable: false, defaultValueSql: "uuid_generate_v4()"),
                    Content     = table.Column<string>(nullable: true),
                    UserID      = table.Column<int>(nullable: false),
                    RoomID      = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Messages", x => x.ID);
                    table.ForeignKey(
                        "FK_Messages_Rooms_RoomID",
                        x => x.RoomID,
                        "Rooms",
                        "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_Messages_Users_UserID",
                        x => x.UserID,
                        "Users",
                        "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "RoomUser",
                table => new
                {
                    UserID = table.Column<int>(nullable: false),
                    RoomID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomUser", x => new {x.UserID, x.RoomID});
                    table.ForeignKey(
                        "FK_RoomUser_Rooms_RoomID",
                        x => x.RoomID,
                        "Rooms",
                        "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_RoomUser_Users_UserID",
                        x => x.UserID,
                        "Users",
                        "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "Likes",
                table => new
                {
                    UserID    = table.Column<int>(nullable: false),
                    MessageID = table.Column<Guid>(nullable: false),
                    DateLiked = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Likes", x => new {x.MessageID, x.UserID});
                    table.ForeignKey(
                        "FK_Likes_Messages_MessageID",
                        x => x.MessageID,
                        "Messages",
                        "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_Likes_Users_UserID",
                        x => x.UserID,
                        "Users",
                        "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                "Rooms",
                new[] {"ID", "DateCreated", "Description", "Disabled", "Name"},
                new object[,]
                {
                    {
                        -1, new DateTime(2019,
                                         12,
                                         1,
                                         18,
                                         29,
                                         27,
                                         321,
                                         DateTimeKind.Local).AddTicks(4224),
                        "Test", false, "Test 1"
                    },
                    {
                        -2, new DateTime(2019,
                                         11,
                                         30,
                                         18,
                                         29,
                                         27,
                                         324,
                                         DateTimeKind.Local).AddTicks(3663),
                        null, false, "Test 2"
                    },
                    {
                        -3, new DateTime(2019,
                                         12,
                                         1,
                                         17,
                                         29,
                                         27,
                                         324,
                                         DateTimeKind.Local).AddTicks(3722),
                        "Test 3", false, "Test 3"
                    }
                });

            migrationBuilder.InsertData(
                "Users",
                "ID",
                new object[]
                {
                    -1,
                    -2,
                    -3
                });

            migrationBuilder.InsertData(
                "RoomUser",
                new[] {"UserID", "RoomID"},
                new object[,]
                {
                    {-1, -3},
                    {-2, -3},
                    {-3, -3}
                });

            migrationBuilder.CreateIndex(
                "IX_Likes_UserID",
                "Likes",
                "UserID");

            migrationBuilder.CreateIndex(
                "IX_Messages_RoomID",
                "Messages",
                "RoomID");

            migrationBuilder.CreateIndex(
                "IX_Messages_UserID",
                "Messages",
                "UserID");

            migrationBuilder.CreateIndex(
                "IX_RoomUser_RoomID",
                "RoomUser",
                "RoomID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "Likes");

            migrationBuilder.DropTable(
                "RoomUser");

            migrationBuilder.DropTable(
                "Messages");

            migrationBuilder.DropTable(
                "Rooms");

            migrationBuilder.DropTable(
                "Users");
        }
    }
}