﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ChatAPI.Migrations
{
    public partial class AddedRoomCloseDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn("Disabled", "Rooms", "Closed");

            migrationBuilder.AddColumn<DateTime>(
                "DateClosed",
                "Rooms",
                nullable: true);

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -3,
                "DateCreated",
                new DateTime(2019,
                             12,
                             1,
                             23,
                             50,
                             22,
                             545,
                             DateTimeKind.Local).AddTicks(2982));

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -2,
                "DateCreated",
                new DateTime(2019,
                             12,
                             1,
                             0,
                             50,
                             22,
                             545,
                             DateTimeKind.Local).AddTicks(2925));

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -1,
                "DateCreated",
                new DateTime(2019,
                             12,
                             2,
                             0,
                             50,
                             22,
                             542,
                             DateTimeKind.Local).AddTicks(2716));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn("Closed", "Rooms", "Disabled");

            migrationBuilder.DropColumn(
                "DateClosed",
                "Rooms");

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -3,
                "DateCreated",
                new DateTime(2019,
                             12,
                             1,
                             23,
                             28,
                             45,
                             992,
                             DateTimeKind.Local).AddTicks(1297));

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -2,
                "DateCreated",
                new DateTime(2019,
                             12,
                             1,
                             0,
                             28,
                             45,
                             992,
                             DateTimeKind.Local).AddTicks(1242));

            migrationBuilder.UpdateData(
                "Rooms",
                "ID",
                -1,
                "DateCreated",
                new DateTime(2019,
                             12,
                             2,
                             0,
                             28,
                             45,
                             989,
                             DateTimeKind.Local).AddTicks(1692));
        }
    }
}