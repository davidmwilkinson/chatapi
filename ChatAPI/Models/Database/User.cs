using System.Collections.Generic;

namespace ChatAPI.Models.Database
{
    public class User
    {
        public int ID { get; set; }

        public IEnumerable<Message> Messages { get; set; }
        public IEnumerable<Like>    Likes    { get; set; }
    }
}