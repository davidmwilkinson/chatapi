using System;
using Microsoft.EntityFrameworkCore;
using Npgsql;

namespace ChatAPI.Models.Database
{
    public class ChatContext : DbContext
    {
        public ChatContext(DbContextOptions<ChatContext> options) : base(options)
        {
        }

        public DbSet<User>    Users    { get; set; }
        public DbSet<Like>    Likes    { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Room>    Rooms    { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(builder =>
            {
                builder.HasMany(x => x.Messages);
                builder.HasMany(x => x.Likes);

                builder.HasData(
                    new User
                    {
                        ID = -1
                    },
                    new User
                    {
                        ID = -2
                    },
                    new User
                    {
                        ID = -3
                    }
                );
            });

            modelBuilder.Entity<Room>(room =>
            {
                room.HasMany(x => x.Messages);
                room.Property(x => x.Name)
                    .IsRequired()
                    .HasMaxLength(Constants.MaxRoomNameLength);
                room.Property(x => x.DateCreated)
                    .ValueGeneratedOnAdd()
                    .HasDefaultValueSql("current_timestamp");
                room.HasData(
                    new Room
                    {
                        Description = "Test",
                        ID          = -1,
                        Name        = "Test 1",
                        DateCreated = DateTime.Now
                    },
                    new Room
                    {
                        Description = null,
                        ID          = -2,
                        Name        = "Test 2",
                        DateCreated = DateTime.Now.Subtract(TimeSpan.FromDays(1))
                    },
                    new Room
                    {
                        Description = "Test 3",
                        ID          = -3,
                        Name        = "Test 3",
                        DateCreated = DateTime.Now.Subtract(TimeSpan.FromHours(1))
                    }
                );
            });

            modelBuilder
                .HasPostgresExtension("uuid-ossp")
                .Entity<Message>(builder =>
                {
                    builder.HasOne(x => x.User)
                        .WithMany(x => x.Messages);
                    builder.HasOne(x => x.Room);

                    builder.Property(x => x.DateCreated)
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("current_timestamp");
                    builder.Property(x => x.ID)
                        .HasDefaultValueSql("uuid_generate_v4()");
                });

            modelBuilder.Entity<Like>(builder =>
            {
                builder.HasOne(x => x.User)
                    .WithMany(x => x.Likes)
                    .HasForeignKey(x => x.UserID);
                builder.HasOne(x => x.Message)
                    .WithMany(x => x.Likes)
                    .HasForeignKey(x => x.MessageID);
                builder.HasKey(x => new {x.MessageID, x.UserID});
                builder.Property(x => x.DateLiked)
                    .ValueGeneratedOnAdd()
                    .HasDefaultValueSql("current_timestamp");
            });
        }
    }
}