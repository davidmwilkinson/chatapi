using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ChatAPI.Models.Database
{
    public class Room
    {
        public int ID { get; set; }

        [MaxLength(Constants.MaxRoomNameLength)]
        public string Name { get; set; }

        public DateTime  DateCreated { get; set; }
        public bool      Closed      { get; set; } = false;
        public DateTime? DateClosed  { get; set; }
        public string    Description { get; set; }

        public IEnumerable<Message> Messages { get; set; }
    }
}