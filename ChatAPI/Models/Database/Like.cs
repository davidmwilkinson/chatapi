using System;

namespace ChatAPI.Models.Database
{
    public class Like
    {
        public DateTime DateLiked { get; set; }
        public int      UserID    { get; set; }
        public Guid     MessageID { get; set; }

        public User    User    { get; set; }
        public Message Message { get; set; }
    }
}