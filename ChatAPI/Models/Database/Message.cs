using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ChatAPI.Models.Database
{
    public class Message
    {
        [Key]
        public Guid ID { get; set; }

        public string   Content     { get; set; }
        public int      UserID      { get; set; }
        public int      RoomID      { get; set; }
        public DateTime DateCreated { get; set; }

        public Room              Room  { get; set; }
        public User              User  { get; set; }
        public IEnumerable<Like> Likes { get; set; }
    }
}