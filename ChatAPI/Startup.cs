using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ChatAPI.Models.Database;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using RabbitMQ.Client;

namespace ChatAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDistributedRedisCache(options => { options.Configuration = Configuration.GetConnectionString("redis"); });

            var configSection = Configuration.GetSection("RabbitMQ");
            var rabbitMqConnectionFactory = new ConnectionFactory()
            {
                UserName    = configSection.GetValue<string>("UserName"),
                Password    = configSection.GetValue<string>("Password"),
                HostName    = configSection.GetValue<string>("HostName"),
                VirtualHost = configSection.GetValue<string>("VirtualHost"),
                Port        = configSection.GetValue<int>("Port")
            };
            services.AddSingleton<IConnectionFactory>(rabbitMqConnectionFactory);

            services.AddHealthChecks()
                .AddDbContextCheck<ChatContext>()
                .AddRedis(Configuration.GetConnectionString("redis"))
                .AddRabbitMQ();

            services.AddDataProtection()
                .PersistKeysToFileSystem(new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                             new OpenApiInfo
                             {
                                 Title   = "Chat API",
                                 Version = "v1"
                             });
                var filePath = Path.Combine(AppContext.BaseDirectory, "ChatAPI.xml");
                c.IncludeXmlComments(filePath);
            });

            services.AddDbContextPool<ChatContext>(builder => { builder.UseNpgsql(Configuration.GetConnectionString("db")); });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Chat API");
                c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health",
                                          new HealthCheckOptions()
                                          {
                                              Predicate      = _ => true,
                                              ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                                          });
                endpoints.MapHealthChecks("/health/self",
                                          new HealthCheckOptions()
                                          {
                                              Predicate      = _ => false,
                                              ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                                          });
            });

            using var scope   = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var       context = scope.ServiceProvider.GetRequiredService<ChatContext>();
            if (env.IsDevelopment()) context.Database.Migrate();
        }
    }
}