# How to run

1. Prerequisites:
    - [Docker](https://docs.docker.com/v17.12/install/)
    - [docker-compose](https://docs.docker.com/compose/install/)
1. From within the ChatAPI folder
1. Run `docker-compose up -d` in the ChatAPI folder
1. Run `docker build -t chatapi:dev .` in the ChatAPI folder
1. Run `docker run --rm -p 5000:5000 --env ASPNETCORE_Environment=Development --network chat chatapi:dev`
1. Navigate to [http://localhost:5000](http://localhost:5000) to see the Swagger page and try out the endpoints
1. Clean up is done by quitting `docker run` instance and executing `docker-compose down -v`

# Building directly on your machine
1. Prerequisites:
    - [.NET Core SDK](https://dotnet.microsoft.com/download)
    - [Docker](https://docs.docker.com/v17.12/install/)
    - [docker-compose](https://docs.docker.com/compose/install/)
1. Run `docker-compose up -d` in the ChatAPI folder
1. You must add a host entry pointing `db` to 127.0.0.1. Otherwise, the application will not be able to find the database and fail on startup.
1. Run `dotnet restore` in the ChatAPI folder
1. Run `dotnet run` in the ChatAPI folder

Note: development and testing was done on Ubuntu 18.04. If you run into problems running it on another system, let me know.