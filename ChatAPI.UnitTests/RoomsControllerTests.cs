using System;
using System.Linq;
using System.Threading.Tasks;
using ChatAPI.Controllers;
using ChatAPI.Models.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq.AutoMock;
using Xunit;

namespace ChatAPI.Tests
{
    public class RoomsControllersTests
    {
        public RoomsControllersTests()
        {
            _mocker = new AutoMocker();
        }

        private readonly AutoMocker _mocker;

        private static readonly Func<string, DbContextOptions<ChatContext>> GenerateOptions =
            TestHelpers.GetDbOptionsGenerator<ChatContext>("RoomsControllerTests");

        [Fact]
        public async Task Test_CloseRoomActuallyClosesRoomAndMarksClosedTime()
        {
            var dbOptions = GenerateOptions("CloseRoomActuallyClosesRoom");

            await using (var context = new ChatContext(dbOptions))
            {
                context.Rooms.AddRange(new Room
                {
                    ID     = 1,
                    Name   = "Test",
                    Closed = false
                });
                context.SaveChanges();
            }

            _mocker.Use(new ChatContext(dbOptions));
            var controller = _mocker.CreateInstance<RoomsController>();

            var response = await controller.CloseRoom(1);

            Assert.IsType<NoContentResult>(response);

            await using (var context = new ChatContext(dbOptions))
            {
                var room = context.Rooms.SingleOrDefault(x => x.ID == 1);

                Assert.NotNull(room);
                Assert.True(room.Closed);
                Assert.NotNull(room.DateClosed);
            }
        }

        [Fact]
        public async Task Test_CloseRoomReturns404IfAlreadyClosed()
        {
            var dbOptions = GenerateOptions("404OnCloseRoomAlreadyClosed");

            await using (var context = new ChatContext(dbOptions))
            {
                context.Rooms.Add(new Room
                {
                    ID     = 5,
                    Closed = true
                });
                context.SaveChanges();
            }

            _mocker.Use(new ChatContext(dbOptions));
            var controller = _mocker.CreateInstance<RoomsController>();

            var response = await controller.CloseRoom(5);

            Assert.IsType<NotFoundResult>(response);
        }

        [Fact]
        public async Task Test_CloseRoomReturns404IfNotFound()
        {
            var dbOptions = GenerateOptions("404OnCloseRoomNotFound");

            _mocker.Use(new ChatContext(dbOptions));
            var controller = _mocker.CreateInstance<RoomsController>();

            var response = await controller.CloseRoom(5);

            Assert.IsType<NotFoundResult>(response);
        }

        [Fact]
        public async Task Test_GetAllRoomsDoesNotRetrieveClosedRoomsByDefault()
        {
            var dbOptions = GenerateOptions("NoClosedRooms");

            await using (var context = new ChatContext(dbOptions))
            {
                context.Rooms.AddRange(
                    new Room(),
                    new Room(),
                    new Room
                    {
                        Closed = true
                    });
                context.SaveChanges();
            }

            _mocker.Use(new ChatContext(dbOptions));

            var controller = _mocker.CreateInstance<RoomsController>();

            var response = await controller.GetAllRooms();

            Assert.NotNull(response.Value);
            Assert.All(response.Value, x => Assert.False(x.Closed));
        }

        [Fact]
        public async Task Test_GetAllRoomsWithIncludeDisabledIncludesDisabledRooms()
        {
            var dbOptions = GenerateOptions("ClosedRoomsIncludedWithSwitch");

            await using (var context = new ChatContext(dbOptions))
            {
                context.Rooms.AddRange(
                    new Room(),
                    new Room(),
                    new Room
                    {
                        Closed = true
                    });
                context.SaveChanges();
            }

            _mocker.Use(new ChatContext(dbOptions));

            var controller = _mocker.CreateInstance<RoomsController>();

            var response = await controller.GetAllRooms(true);

            Assert.NotNull(response.Value);
            Assert.Contains(response.Value, output => output.Closed);
        }

        [Fact]
        public async Task Test_GetRoomReturns404IfNotFound()
        {
            var dbOptions = GenerateOptions("404OnNoMatchingRoom");

            _mocker.Use(new ChatContext(dbOptions));
            var controller = _mocker.CreateInstance<RoomsController>();

            var response = await controller.GetRoom(1);

            Assert.NotNull(response.Result);
            Assert.IsType<NotFoundResult>(response.Result);
        }

        [Fact]
        public async Task Test_GetRoomReturnsMatchingRoom()
        {
            var dbOptions = GenerateOptions("GetMatchingRoom");

            await using (var context = new ChatContext(dbOptions))
            {
                context.Rooms.AddRange(new Room
                                       {
                                           ID = 28
                                       },
                                       new Room
                                       {
                                           ID   = 50,
                                           Name = "This"
                                       },
                                       new Room
                                       {
                                           ID = 10
                                       });
                context.SaveChanges();
            }

            _mocker.Use(new ChatContext(dbOptions));
            var controller = _mocker.CreateInstance<RoomsController>();

            var response = await controller.GetRoom(50);

            Assert.NotNull(response.Value);
            Assert.Equal(50,     response.Value.ID);
            Assert.Equal("This", response.Value.Name);
        }

        [Fact]
        public async Task Test_MultipleRoomsWithSameNamePermitted()
        {
            var dbOptions = GenerateOptions("MultipleRoomsSameName");

            await using (var context = new ChatContext(dbOptions))
            {
                context.Rooms.AddRange(new Room
                {
                    Name = "Test"
                });
                context.SaveChanges();
            }

            _mocker.Use(new ChatContext(dbOptions));
            var controller = _mocker.CreateInstance<RoomsController>();

            var response = await controller.CreateRoom(new RoomsController.CreateRoomInput
            {
                Name = "Test"
            });

            Assert.IsType<CreatedAtRouteResult>(response);
        }
    }
}