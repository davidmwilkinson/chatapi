using System;
using System.Linq;
using System.Threading.Tasks;
using ChatAPI.Controllers;
using ChatAPI.Models.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq.AutoMock;
using Xunit;

namespace ChatAPI.Tests
{
    public class MessagesControllerTests
    {
        public MessagesControllerTests()
        {
            _mocker = new AutoMocker();
        }

        private readonly AutoMocker _mocker;

        private static readonly Func<string, DbContextOptions<ChatContext>> GenerateOptions =
            TestHelpers.GetDbOptionsGenerator<ChatContext>("MessagesControllerTests");


        [Fact]
        public async Task Test_CreateMessageActuallyCreatesMessage()
        {
            var dbOptions = GenerateOptions("CreateMessageActuallyCreatesMessage");
            TestHelpers.AddUsers(dbOptions);
            TestHelpers.AddRooms(dbOptions);

            int numberOfMessages;

            await using (var context = new ChatContext(dbOptions))
            {
                context.Messages.Add(new Message
                {
                    ID          = Guid.Parse("b8f45965-7ea7-4c79-bfd2-69fa29cad1d8"),
                    RoomID      = 1,
                    UserID      = 1,
                    Content     = "test",
                    DateCreated = DateTime.Now.Subtract(TimeSpan.FromMinutes(5))
                });
                numberOfMessages = context.Messages.Count();
            }

            _mocker.Use(new ChatContext(dbOptions));

            var controller = _mocker.CreateInstance<MessagesController>();

            var response = await controller.CreateMessage(new MessagesController.MessageInput
            {
                Content = "blah",
                RoomID  = 1,
                UserID  = 1
            });
            
            Assert.NotNull(response.Value);

            await using (var context = new ChatContext(dbOptions))
            {
                var insertedMessage = context.Messages
                    .SingleOrDefaultAsync(x => x.ID != Guid.Parse("b8f45965-7ea7-4c79-bfd2-69fa29cad1d8"));
                Assert.NotNull(insertedMessage);
                Assert.Equal(numberOfMessages + 1, context.Messages.Count());
            }
        }

        [Fact]
        public async Task Test_CreateMessageReturns400OnBadRoomID()
        {
            var dbOptions = GenerateOptions("CreateMessage400OnBadRoomID");
            TestHelpers.AddUsers(dbOptions);
            TestHelpers.AddRooms(dbOptions);

            _mocker.Use(new ChatContext(dbOptions));

            var controller = _mocker.CreateInstance<MessagesController>();

            var response = await controller.CreateMessage(new MessagesController.MessageInput
            {
                Content = "blah",
                RoomID  = 1000,
                UserID  = 1
            });

            Assert.IsType<BadRequestObjectResult>(response.Result);
            Assert.IsType<ProblemDetails>(((BadRequestObjectResult) response.Result).Value);
        }

        [Fact]
        public async Task Test_CreateMessageReturns400OnBadUserID()
        {
            var dbOptions = GenerateOptions("CreateMessage400OnBadUserID");
            TestHelpers.AddUsers(dbOptions);
            TestHelpers.AddRooms(dbOptions);

            _mocker.Use(new ChatContext(dbOptions));

            var controller = _mocker.CreateInstance<MessagesController>();

            var response = await controller.CreateMessage(new MessagesController.MessageInput
            {
                Content = "blah",
                RoomID  = 1,
                UserID  = 1000
            });

            Assert.IsType<BadRequestObjectResult>(response.Result);
            Assert.IsType<ProblemDetails>(((BadRequestObjectResult) response.Result).Value);
        }

        [Fact]
        public async Task Test_GetMessagesFromRoomReturns400OnBadRoomID()
        {
            var dbOptions = GenerateOptions("GetMessagesReturns400OnBadRoomID");
            TestHelpers.AddUsers(dbOptions);
            TestHelpers.AddRooms(dbOptions);

            _mocker.Use(new ChatContext(dbOptions));

            var controller = _mocker.CreateInstance<MessagesController>();

            var response = await controller.GetRoomMessages(5000);

            Assert.IsType<BadRequestObjectResult>(response.Result);
        }

        [Fact]
        public async Task Test_GetMessagesWithoutBeforeOrAfterReturnsAllMessages()
        {
            var dbOptions = GenerateOptions("GetAllMessagesInRoomReturnsAllMessages");

            TestHelpers.AddUsers(dbOptions);
            TestHelpers.AddRooms(dbOptions);
            TestHelpers.AddMessages(dbOptions);

            _mocker.Use(new ChatContext(dbOptions));

            var controller = _mocker.CreateInstance<MessagesController>();

            var response = await controller.GetRoomMessages(1);

            Assert.NotNull(response.Value);
            Assert.Collection(response.Value,
                              x => Assert.Equal("test1", x.Content),
                              x => Assert.Equal("test2", x.Content),
                              x => Assert.Equal("test3", x.Content)
            );
        }

        [Fact]
        public async Task Test_AfterFlagFetchesOnlyMessagesAfterGivenTime()
        {
            var dbOptions = GenerateOptions("AfterFlagWorks");

            TestHelpers.AddUsers(dbOptions);
            TestHelpers.AddRooms(dbOptions);
            TestHelpers.AddMessages(dbOptions);

            _mocker.Use(new ChatContext(dbOptions));

            var controller = _mocker.CreateInstance<MessagesController>();

            var response = await controller.GetRoomMessages(1, after: DateTime.Now.Subtract(TimeSpan.FromMinutes(6)));

            Assert.NotNull(response.Value);
            Assert.Collection(response.Value,
                              x => Assert.Equal("test1", x.Content),
                              x => Assert.Equal("test2", x.Content));
        }

        [Fact]
        public async Task Test_BeforeFlagFetchesOnlyMessagesBeforeGivenTime()
        {
            var dbOptions = GenerateOptions("BeforeFlagWorks");

            TestHelpers.AddUsers(dbOptions);
            TestHelpers.AddRooms(dbOptions);
            TestHelpers.AddMessages(dbOptions);

            _mocker.Use(new ChatContext(dbOptions));

            var controller = _mocker.CreateInstance<MessagesController>();

            var response = await controller.GetRoomMessages(1, before: DateTime.Now.Subtract(TimeSpan.FromMinutes(3)));

            Assert.NotNull(response.Value);
            Assert.Collection(response.Value,
                              x => Assert.Equal("test2", x.Content),
                              x => Assert.Equal("test3", x.Content));
        }

        [Fact]
        public async Task Test_BeforeAndAfterFlagsWorkWhenUsedTogether()
        {
            var dbOptions = GenerateOptions("FlagsWorkTogether");

            TestHelpers.AddUsers(dbOptions);
            TestHelpers.AddRooms(dbOptions);
            TestHelpers.AddMessages(dbOptions);

            _mocker.Use(new ChatContext(dbOptions));

            var controller = _mocker.CreateInstance<MessagesController>();

            var response = await controller.GetRoomMessages(1,
                                                            after: DateTime.Now.Subtract(TimeSpan.FromMinutes(6)),
                                                            before: DateTime.Now.Subtract(TimeSpan.FromMinutes(3)));

            Assert.NotNull(response.Value);
            Assert.Collection(response.Value,
                              x => Assert.Equal("test2", x.Content));
        }

        [Fact]
        public async Task Test_InvalidDateRangeReturns400()
        {
            var dbOptions = GenerateOptions("InvalidDateRangeReturns400");

            TestHelpers.AddUsers(dbOptions);
            TestHelpers.AddRooms(dbOptions);
            TestHelpers.AddMessages(dbOptions);

            _mocker.Use(new ChatContext(dbOptions));

            var controller = _mocker.CreateInstance<MessagesController>();

            var response = await controller.GetRoomMessages(1,
                                                            after: DateTime.Now.Subtract(TimeSpan.FromMinutes(1)),
                                                            before: DateTime.Now.Subtract(TimeSpan.FromMinutes(3)));

            Assert.IsType<BadRequestObjectResult>(response.Result);
        }
    }
}