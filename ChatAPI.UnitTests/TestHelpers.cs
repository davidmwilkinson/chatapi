using System;
using ChatAPI.Models.Database;
using Microsoft.EntityFrameworkCore;

namespace ChatAPI.Tests
{
    public static class TestHelpers
    {
        public static Func<string, DbContextOptions<T>> GetDbOptionsGenerator<T>(string baseName)
            where T : DbContext
        {
            return s => new DbContextOptionsBuilder<T>()
                .UseInMemoryDatabase($"{baseName}_{s}")
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors()
                .Options;
        }

        public static void AddUsers(DbContextOptions<ChatContext> options)
        {
            using var context = new ChatContext(options);

            context.Users.AddRange(
                new User
                {
                    ID = 1
                },
                new User
                {
                    ID = 2
                });
            context.SaveChanges();
        }

        public static void AddRooms(DbContextOptions<ChatContext> options)
        {
            using var context = new ChatContext(options);

            context.Rooms.AddRange(new Room
                                   {
                                       ID   = 1,
                                       Name = "test 1"
                                   },
                                   new Room
                                   {
                                       ID          = 2,
                                       Name        = "test 2",
                                       Description = "desc"
                                   },
                                   new Room
                                   {
                                       ID     = 3,
                                       Closed = true
                                   }
            );
            context.SaveChanges();
        }

        public static void AddMessages(DbContextOptions<ChatContext> options)
        {
            using var context = new ChatContext(options);

            context.Messages.AddRange(new Message
                                      {
                                          ID          = Guid.Parse("b8f45965-7ea7-4c79-bfd2-69fa29cad1d8"),
                                          RoomID      = 1,
                                          UserID      = 1,
                                          Content     = "test1",
                                          DateCreated = DateTime.Now
                                      },
                                      new Message
                                      {
                                          ID          = Guid.Parse("4c22b894-374f-4b00-bd46-af322d14eb33"),
                                          RoomID      = 1,
                                          UserID      = 1,
                                          Content     = "test2",
                                          DateCreated = DateTime.Now.Subtract(TimeSpan.FromMinutes(5))
                                      },
                                      new Message
                                      {
                                          ID          = Guid.Parse("2fa05cc6-52c8-46e5-b2a1-c942b8dee7ef"),
                                          RoomID      = 1,
                                          UserID      = 2,
                                          Content     = "test3",
                                          DateCreated = DateTime.Now.Subtract(TimeSpan.FromMinutes(10))
                                      });
            context.SaveChanges();
        }
    }
}