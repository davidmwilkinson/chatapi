using System;
using System.Threading.Tasks;
using ChatAPI.Controllers;
using ChatAPI.Models.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Moq.AutoMock;
using Xunit;

namespace ChatAPI.Tests
{
    public class LikesControllerTests
    {
        public LikesControllerTests()
        {
            _mocker = new AutoMocker();
        }

        private readonly AutoMocker _mocker;

        private static readonly Func<string, DbContextOptions<ChatContext>> GenerateOptions =
            TestHelpers.GetDbOptionsGenerator<ChatContext>("LikesControllerTests");

        [Fact]
        public async Task Test_AddLikeReturns400IfMessageDoesNotExist()
        {
            var dbOptions = GenerateOptions("AddLikeGives400ForBadMessageID");

            _mocker.Use(new ChatContext(dbOptions));

            var controller = _mocker.CreateInstance<LikesController>();

            var response = await controller.AddLike(new LikesController.LikeInput
            {
                MessageID = Guid.Parse("E6C5CB2C-A184-4589-805C-1F3D4DC71CA2"),
                UserID    = 1
            });

            Assert.IsType<BadRequestObjectResult>(response);
        }

        [Fact]
        public async Task Test_AddLikeReturns400IfUserDoesNotExist()
        {
            var dbOptions = GenerateOptions("AddLikeGives400ForBadUser");

            TestHelpers.AddRooms(dbOptions);
            TestHelpers.AddUsers(dbOptions);
            TestHelpers.AddMessages(dbOptions);

            _mocker.Use(new ChatContext(dbOptions));

            var controller = _mocker.CreateInstance<LikesController>();

            var response = await controller.AddLike(new LikesController.LikeInput
            {
                MessageID = Guid.Parse("b8f45965-7ea7-4c79-bfd2-69fa29cad1d8"),
                UserID    = 1000
            });

            Assert.IsType<BadRequestObjectResult>(response);
        }

        [Fact]
        public async Task Test_AddLikeActuallyAddsLike()
        {
            var dbOptions = GenerateOptions("AddLikeActuallyAddsLike");

            TestHelpers.AddRooms(dbOptions);
            TestHelpers.AddUsers(dbOptions);
            TestHelpers.AddMessages(dbOptions);

            _mocker.Use(new ChatContext(dbOptions));

            var controller = _mocker.CreateInstance<LikesController>();

            var response = await controller.AddLike(new LikesController.LikeInput
            {
                MessageID = Guid.Parse("b8f45965-7ea7-4c79-bfd2-69fa29cad1d8"),
                UserID    = 1
            });

            Assert.IsType<NoContentResult>(response);

            await using (var context = new ChatContext(dbOptions))
            {
                var like = context.Likes.SingleOrDefault(x => x.UserID == 1
                                                              && x.MessageID == Guid.Parse("b8f45965-7ea7-4c79-bfd2-69fa29cad1d8"));
                Assert.NotNull(like);
            }
        }

        [Fact]
        public async Task Test_AddLikeReturnsBadRequestIfAlreadyExists()
        {
            var dbOptions = GenerateOptions("AddLikeIsIdempotent");

            TestHelpers.AddRooms(dbOptions);
            TestHelpers.AddUsers(dbOptions);
            TestHelpers.AddMessages(dbOptions);

            _mocker.Use(new ChatContext(dbOptions));

            var input = new LikesController.LikeInput
            {
                MessageID = Guid.Parse("b8f45965-7ea7-4c79-bfd2-69fa29cad1d8"),
                UserID    = 1
            };

            await using (var context = new ChatContext(dbOptions))
            {
                context.Likes.Add(new Like
                {
                    MessageID = input.MessageID,
                    UserID    = input.UserID
                });
                context.SaveChanges();
            }

            var controller = _mocker.CreateInstance<LikesController>();

            var response = await controller.AddLike(input);

            Assert.IsType<BadRequestObjectResult>(response);

            await using (var context = new ChatContext(dbOptions))
            {
                var matchingLikes = context.Likes.Where(x => x.UserID == 1
                                                             && x.MessageID == Guid.Parse("b8f45965-7ea7-4c79-bfd2-69fa29cad1d8"))
                    .ToList();
                Assert.Single(matchingLikes);
            }
        }

        [Fact]
        public async Task Test_DeleteLikeActuallyDeletesLike()
        {
            var dbOptions = GenerateOptions("DeleteLikeDeletesLike");

            TestHelpers.AddRooms(dbOptions);
            TestHelpers.AddUsers(dbOptions);
            TestHelpers.AddMessages(dbOptions);

            _mocker.Use(new ChatContext(dbOptions));

            var input = new LikesController.LikeInput
            {
                MessageID = Guid.Parse("b8f45965-7ea7-4c79-bfd2-69fa29cad1d8"),
                UserID    = 1
            };

            await using (var context = new ChatContext(dbOptions))
            {
                context.Likes.Add(new Like
                {
                    MessageID = input.MessageID,
                    UserID    = input.UserID
                });
                context.SaveChanges();
            }

            var controller = _mocker.CreateInstance<LikesController>();

            var response = await controller.DeleteLike(input);

            Assert.IsType<NoContentResult>(response);

            await using (var context = new ChatContext(dbOptions))
            {
                var matchingLikes = context.Likes.Where(x => x.UserID == 1
                                                             && x.MessageID == Guid.Parse("b8f45965-7ea7-4c79-bfd2-69fa29cad1d8"))
                    .ToList();
                Assert.Empty(matchingLikes);
            }
        }

        [Fact]
        public async Task Test_DeleteLikeReturns404IfNoLikeExists()
        {
            var dbOptions = GenerateOptions("DeleteIsIdempotent");

            TestHelpers.AddRooms(dbOptions);
            TestHelpers.AddUsers(dbOptions);
            TestHelpers.AddMessages(dbOptions);

            _mocker.Use(new ChatContext(dbOptions));

            var input = new LikesController.LikeInput
            {
                MessageID = Guid.Parse("b8f45965-7ea7-4c79-bfd2-69fa29cad1d8"),
                UserID    = 1
            };

            var controller = _mocker.CreateInstance<LikesController>();

            var response = await controller.DeleteLike(input);

            Assert.IsType<NotFoundResult>(response);
        }
    }
}